using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Non_Rotated : MonoBehaviour
{
    [SerializeField]
    private Vector3 rotation = Vector3.zero;

    private void FixedUpdate()
    {
        this.transform.rotation = Quaternion.Euler( rotation );
    }
}
