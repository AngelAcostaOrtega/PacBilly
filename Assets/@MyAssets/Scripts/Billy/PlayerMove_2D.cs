using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]


public class PlayerMove_2D : MonoBehaviour
{
    [SerializeField][Range(1, 10)]
    private float Speed = 1;
    private void OnEnable()
    {
        Vector3 sourcePostion = new Vector3(0f, -1.25f, 0f); //The position you want to place your agent
        NavMeshHit closestHit;
        if (NavMesh.SamplePosition(sourcePostion, out closestHit, 500, 1))
        {
            this.transform.position = closestHit.position;
            //TODO
        }
        else
        {
            Debug.Log("El jugador no se pudo posicionar en la malla.");
        }
    }
    private void Start()
    {
        this.transform.rotation = Quaternion.identity;
    }

    void FixedUpdate()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");

        if (x != 0 || y != 0)
        {
            Vector2 Direction = new Vector2(x, y);
            RaycastHit2D ray = Physics2D.Raycast(this.transform.position, Direction);
            if(ray.collider != null)
            {
                if(ray.collider.gameObject.CompareTag("Wall"))
                {
                    setDestinationTo(ray.point);
                    Debug.DrawRay(this.transform.position, Direction, Color.red, 40f);
                }
            }
            
        }
    }

    private void setDestinationTo(Vector2 position)
    {
        if(this.TryGetComponent(out NavMeshAgent agent))
        {
            agent.SetDestination(position);
        }
    }
}
