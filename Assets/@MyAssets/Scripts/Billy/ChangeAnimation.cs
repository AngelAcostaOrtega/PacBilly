using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ChangeAnimation : MonoBehaviour
{
    void Update()
    {
        if(this.TryGetComponent(out NavMeshAgent agent))
        {
            ChangeIsWalkingAnimation(agent.velocity != Vector3.zero);
        }
    }

    private void ChangeIsWalkingAnimation(bool value)
    {
        if (this.TryGetComponent(out Animator animator))
        {
            if (animator.GetBool("isWalking") != value)
            {
                animator.SetBool("isWalking", value);
            }
        }
        else if (this.transform.GetChild(0).TryGetComponent(out Animator animatorChild))
        {
            if (animatorChild.GetBool("isWalking") != value)
            {
                animatorChild.SetBool("isWalking", value);
            }
        }
    }
}
