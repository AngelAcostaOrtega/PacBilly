using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager instance = null;
    public static GameManager Instance { get { return instance; } }

    private int chipCount = -1;
    private int ChipCount { get { return chipCount; } }
    private int gettedChips = 0;
    public int GettedChips { get { return gettedChips; } }
    public float ChipsPercentage { get { return (100 * GettedChips) / ChipCount; } }
    private void Start()
    {
        MakeSingleton();

        chipCount = GameObject.FindObjectsOfType<OnPlayerApproach>().Length;
        //Debug.Log(chipCount + "");

        changePointGUI();
    }

    private void MakeSingleton()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this, 0.1f);
        }
    }

    public void sumPoint()
    {
        gettedChips++;
        changePointGUI();
    }
    private void changePointGUI()
    {
        GameObject label = GameObject.Find("Canvas/InGameGUI/Container/ChipInfo/ChipText");
        if(label != null)
        {
            if(label.TryGetComponent(out TMPro.TextMeshProUGUI tmpro))
            {
                tmpro.text = "-" + gettedChips + "/" + chipCount + "c";
            }
        }
        if(gettedChips == chipCount) Win();
    }

    public void Win()
    {
        changeToFinishScreen("�Has ganado!");
    }

    public void Lose()
    {
        changeToFinishScreen("Has perdido");
    }

    private void changeToFinishScreen(string Title)
    {
        GameObject inGameGUI = GameObject.Find("Canvas/InGameGUI");
        GameObject inFinishGUI = GameObject.Find("Canvas/InFinishGUI");

        if( inGameGUI != null && inFinishGUI != null )
        {
            inGameGUI.transform.GetChild(0).gameObject.SetActive(false);
            inFinishGUI.transform.GetChild(0).gameObject.SetActive(true);

            GameObject titleText = inFinishGUI.transform.Find("Container/Panel/Title").gameObject;

            if(titleText != null)
            {
                if(titleText.TryGetComponent(out TMPro.TextMeshProUGUI tmpro))
                {
                    tmpro.text = Title;
                }
            }
        }
    }

}
