using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transformation : MonoBehaviour
{
    [SerializeField]
    private Sprite postTranformationForm = null;
    public void OnTransformation()
    {
        GameObject face = this.transform.Find("Head/Face").gameObject;
        if(face != null)
        {
            if(face.TryGetComponent(out TMPro.TextMeshPro textMeshPro))
            {
                textMeshPro.text = "}:[";
            }
        }
        GameObject particles = this.transform.Find("Head/PreExplosion/Particles").gameObject;
        if(particles != null)
        {
            if(particles.TryGetComponent(out ParticleSystem particleSystem))
            {
                particleSystem.Play();
            }
        }

    }

    public void OnPostTranformation()
    {
        GameObject head = this.transform.GetChild(0).gameObject;
        GameObject leftLeg = this.transform.GetChild(1).gameObject;
        GameObject rightLeg = this.transform.GetChild(2).gameObject;

        if(head != null && leftLeg != null && rightLeg != null)
        {
            if(head.TryGetComponent(out SpriteRenderer spriteRenderer))
            {
                if(postTranformationForm != null)
                {
                    spriteRenderer.sprite = postTranformationForm;
                }
            }

            leftLeg.SetActive(false);
            rightLeg.SetActive(false);

            if(this.transform.parent != null)
            {
                if(this.transform.parent.TryGetComponent(out Pursuit pursuit))
                {
                    pursuit.ChangeToMonster();
                }
            }
        }
    }

    public void TransformAnimation()
    {
        if(this.TryGetComponent(out Animator anim))
        {
            anim.SetBool("isMonster", true);
        }
    }
    public void ChangeWalkingAnimation(bool value)
    {
        if (this.TryGetComponent(out Animator anim))
        {
            anim.SetBool("isWalking", value);
        }
    }
}
