using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Pursuit : MonoBehaviour
{
    [HideInInspector]
    public bool isMonster = false;

    private GameObject player = null;

    private void OnEnable()
    {
        GameObject tempPlayer = GameObject.FindGameObjectWithTag("Player");
        if(tempPlayer != null)
        {
            player = tempPlayer;
        }

        Vector3 sourcePostion = new Vector3(0f, 0.6f, 0f);
        NavMeshHit closestHit;
        if (NavMesh.SamplePosition(sourcePostion, out closestHit, 500, 1))
        {
            this.transform.position = closestHit.position;
            //TODO
            StartCoroutine( StartPursuit() );
        }
        else
        {
            Debug.Log("Enemigo " + this.gameObject.name + " no se pudo posicionar en la malla.");
        }
    }

    private void Update()
    {
        if(player != null)
        {
            if(Vector2.Distance(this.transform.position, player.transform.position) < 0.5f)
            {
                GameManager.Instance.Lose();
            }
        }
        if(GameManager.Instance != null)
        {
            if(GameManager.Instance.ChipsPercentage > 50 && !isMonster)
            {
                if (this.TryGetComponent(out NavMeshAgent agent))
                {
                    agent.SetDestination(this.transform.position);
                }
                if ( this.transform.GetChild(0).TryGetComponent(out Transformation trans) )
                {
                    trans.TransformAnimation();
                }
            }
        }
    }

    private IEnumerator StartPursuit()
    {
        yield return new WaitForSecondsRealtime(2f);
        StartCoroutine( InfiniteMovement() );
    }

    private IEnumerator InfiniteMovement()
    {
        if (this.TryGetComponent(out NavMeshAgent agent))
        {
            if (Vector2.Distance(this.transform.position, agent.pathEndPosition) < 0.5f)
            {
                setDestinationToPlayer();
            }
            if (this.transform.GetChild(0).TryGetComponent(out Transformation trans))
            {
                trans.ChangeWalkingAnimation( agent.velocity != Vector3.zero );
            }
        }
        if(isMonster) yield return new WaitForSecondsRealtime(0.5f);
        else yield return new WaitForSecondsRealtime(1.5f);
        StartCoroutine( InfiniteMovement() );
    }


    private void setDestinationToPlayer()
    {
        if (this.TryGetComponent(out NavMeshAgent agent))
        {
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            if(player != null)
            {
                agent.SetDestination(player.transform.position);
            }
        }
    }

    public void ChangeToMonster()
    {
        isMonster = true;
        if (this.TryGetComponent(out NavMeshAgent agent))
        {
            agent.acceleration = agent.acceleration * 1.5f;
        }
    }
}
