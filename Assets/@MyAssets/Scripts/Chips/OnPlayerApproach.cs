using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnPlayerApproach : MonoBehaviour
{
    private GameObject player = null;

    private void OnEnable()
    {
        GameObject tempPlayer = GameObject.FindGameObjectWithTag("Player");
        if(tempPlayer != null)
        {
            player = tempPlayer;
        }
    }

    private void FixedUpdate()
    {
        if(player != null)
        {
            if(Vector2.Distance(player.transform.position, this.transform.position) < .5f)
            {
                OnChipGetted();
            }
        }
    }

    private void OnChipGetted()
    {
        GameManager.Instance.sumPoint();
        Destroy(this.gameObject);
    }
}
